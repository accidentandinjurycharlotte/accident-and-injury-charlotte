We are a team of Chiropractic Care experts with more than 40 years of experience treating accident injury victims in the Charlotte area.

Our team includes a combination of Chiropractic Doctors and Assistants who use their extensive training to releive the pain our clients are experiencing.

We provide comprehensive chiropractic care to patients who were injured in auto accidents which caused them pain to their neck and back. 

Our fully licensed team of chiropractors uses adjustment techniques and specialized physical therapy to create pain-free lifestyles.


Address : 5410 North Tryon Street, Charlotte, NC 28213

Phone : 704-612-8080
